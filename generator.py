#!/usr/bin/env python

import dominate
from dominate import tags
from dominate.util import raw
import yaml

def setup():
    languages = list(yaml.load_all(open('languages.yaml'), Loader=yaml.SafeLoader))[0]
    mlwordles = list(yaml.load_all(open('mlwordles.yaml'), Loader=yaml.SafeLoader))[0]
    domwordles = list(yaml.load_all(open('eng-domain.yaml'), Loader=yaml.SafeLoader))[0]
    twistwordles = list(yaml.load_all(open('eng-twist.yaml'), Loader=yaml.SafeLoader))[0]
    reimplwordles = list(yaml.load_all(open('eng-reimplementations.yaml'), Loader=yaml.SafeLoader))[0]
    nonlingwordles = list(yaml.load_all(open('nonlinguistic.yaml'), Loader=yaml.SafeLoader))[0]
    langs = {}
    for language in languages:
        langs[language['iso']] = {
            'name': language['name'],
            'name-en': language['name-en'],
            'wiki-en': language['wiki-en'],
            }
        if 'css' in language:
            langs[language['iso']]['css'] = language['css']
        if 'wiki' in language:
            langs[language['iso']]['wiki'] = language['wiki']
        if 'wiki-lang' in language:
            langs[language['iso']]['wiki-lang'] = language['wiki-lang']
    return langs, mlwordles, domwordles, twistwordles, reimplwordles, nonlingwordles

def header(doc):
    with doc.head:
        #  link(rel='stylesheet', href='mini-default.min.css')
        #  link(rel='stylesheet', href='pure-min.css')
        tags.link(rel='stylesheet', href="https://unpkg.com/purecss@2.0.6/build/pure-min.css", integrity="sha384-Uu6IeWbM+gzNVXJcM9XV3SohHtmWE+3VGi496jvgX1jyvDTXfdK+rfZc8C1Aehk5", crossorigin="anonymous")
        tags.meta(name="viewport", content="width=device-width, initial-scale=1")
        tags.link(rel='stylesheet', href='style.css')
        raw('''
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Gentium+Basic:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet"> 
                ''')
        tags.script(src="https://twemoji.maxcdn.com/v/latest/twemoji.min.js", crossorigin="anonymous")
        tags.link(rel='icon', href='logo.png')
        tags.meta(name="description", content="A multilingual ever-growing list of Wordles from all over the world 🟩🟩🟩🟩🟩")
        tags.meta(name="keywords", content="Wordle, game, list, multilingual, word, games, multilingualism, words")
        tags.meta(name="author", content="Júda Ronén")
        tags.meta(property="og:title", content="🟩 Wordles of the World")
        tags.meta(property="og:type", content="website")
        tags.meta(property="og:image", content="https://rwmpelstilzchen.gitlab.io/wordles/wordle.png")
        tags.meta(property="og:url", content="https://rwmpelstilzchen.gitlab.io/wordles/")
        tags.meta(property="description", content="A multilingual ever-growing list of Wordles from all over the world 🟩🟩🟩🟩🟩")
        tags.meta(name="twitter:card", content="summary_large_image")
        tags.meta(name="twitter:creator", content="@judaronen")

def toptext(langs, mlwordles, domwordles, twistwordles, reimplwordles, nonlingwordles):
    tags.h1("🟩 Wordles of the world, unite!")
    with tags.p(__pretty=False):
        tags.span("This is — to the best of my knowledge — the most comprehensive list of ")
        tags.a("Wordle", href="https://www.powerlanguage.co.uk/wordle/")
        tags.span("-like games online. It currently contains ")
        tags.span(str(len(mlwordles) + len(domwordles) + len(twistwordles) + len(reimplwordles) + 1))
        tags.span(" entries in ")
        tags.span(str(len(langs)))
        tags.span(" languages. Do you know of any clone or version that is absent from the list? Please ")
        tags.a("contact me", href="http://me.digitalwords.net/")
        tags.span(" or make a ")
        with tags.a(href="https://gitlab.com/rwmpelstilzchen/wordles"):
            tags.span("pull request ")
            tags.img(src='gitlab.svg', cls='icon')
        tags.span(" and I will be happy to add it 🙂. Also, if you have made a wordle in any language and would like to link here that would be great, as it would create a network of wordles.")
    with tags.p(__pretty=False):
        tags.span("This list is based on lists by ")
        tags.a("Kurt", href="https://github.com/thiskurt/wordle-languages")
        tags.span(", ")
        tags.a("Sam", href="https://gist.github.com/settinger/7dbadc5646b8f58e4f29562305af03fe")
        tags.span(", ")
        tags.a("@yeahwhyyes", href="https://twitter.com/yeahwhyyes/status/1484512915491282950")
        tags.span(", ")
        tags.a("@cuffedCatling", href="https://twitter.com/cuffedCatling/status/1483764561434103811")
        tags.span(" and ")
        tags.a("@omionabike", href="https://twitter.com/omionabike/status/1485558652618833921")
        tags.span(", as well as other versions I came upon or were suggested to me (e.g. ")
        tags.a("here", href="https://twitter.com/JudaRonen/status/1484274702273105921")
        tags.span(", by ")
        tags.a("@jaaaarwr", href="https://twitter.com/jaaaarwr")
        tags.span(" et al.).")
    with tags.p(__pretty=False):
        tags.span("Read more about multilingual Wordle on ")
        tags.a("Rest of World", href="https://restofworld.org/2022/wordle-viral-turkish-japanese-tamil-portuguese/")
        tags.span(", ")
        tags.a("Duolingo", href="https://blog.duolingo.com/wordle-in-other-languages/")
        tags.span(" and ")
        tags.a("Wikipedia", href="https://en.wikipedia.org/wiki/Wordle#Adaptations_and_clones")
        tags.span(".")

def tab_wordle(wordle):
    if 'css' in wordle:
        tags.td(tags.a(wordle['name'], href=wordle['url']), cls=wordle['css'])
    else:
        tags.td(tags.a(wordle['name'], href=wordle['url']))

def tab_code(wordle):
    if 'src' in wordle:
        if wordle['src-type'] == 'github':
            tags.td(tags.a(tags.img(src='github.svg', cls='icon'), href=wordle['src']))
        elif wordle['src-type'] == 'git':
            tags.td(tags.a(tags.img(src='git.svg', cls='icon'), href=wordle['src']))
        elif wordle['src-type'] == 'other':
            tags.td(tags.a(tags.img(src='code.svg', cls='icon'), href=wordle['src']))
    else:
        tags.td()

def tab_note(wordle):
    if 'note' in wordle:
        tags.td(wordle['note'])
    else:
        tags.td()

def tab_media(wordle):
    if 'media' in wordle:
        with tags.td():
            for medium in wordle['media']:
                tags.a("🔗", href=medium['url'])
    else:
        tags.td()

def mltable(langs, wordles):
    with tags.table(cls='pure-table mltable'):
        tags.caption("🌐 Multilingual wordles")
        with tags.thead():
            with tags.tr():
                tags.th("Language", colspan='2')
                tags.th("Name")
                tags.th("Code")
                tags.th("Note")
                tags.th("Media")
        prevlang = ''
        with tags.tbody():
            for wordle in wordles:
                with tags.tr():
                    if prevlang != wordle['lang']:
                        prevlang = wordle['lang']
                        tags.td(tags.a(
                            langs[wordle['lang']]['name-en'],
                            href="https://en.wikipedia.org/wiki/" + langs[wordle['lang']]['wiki-en'],
                            cls="lowkeylink"
                            ))
                        if 'css' in langs[wordle['lang']]:
                            if 'wiki' in langs[wordle['lang']]:
                                tags.td(tags.a(
                                    langs[wordle['lang']]['name'],
                                    href="https://" + langs[wordle['lang']]['wiki-lang'] + ".wikipedia.org/wiki/" + langs[wordle['lang']]['wiki'],
                                    cls="lowkeylink"
                                    ), cls=langs[wordle['lang']]['css'])
                            else:
                                tags.td(langs[wordle['lang']]['name'], cls=langs[wordle['lang']]['css'])
                        else:
                            if 'wiki' in langs[wordle['lang']]:
                                tags.td(tags.a(
                                    langs[wordle['lang']]['name'],
                                    href="https://" + langs[wordle['lang']]['wiki-lang'] + ".wikipedia.org/wiki/" + langs[wordle['lang']]['wiki'],
                                    cls="lowkeylink"
                                    ))
                            else:
                                tags.td(langs[wordle['lang']]['name'])
                    else:
                        tags.td()
                        tags.td()
                    tab_wordle(wordle)
                    tab_code(wordle)
                    tab_note(wordle)
                    tab_media(wordle)

def eng_domain_specific(wordles):
    with tags.table(cls='pure-table domaintable'):
        tags.caption("📖 English domain-specific wordles")
        with tags.thead():
            with tags.tr():
                tags.th("Name")
                tags.th("Code")
                tags.th("Emoji")
                tags.th("Domain")
                tags.th("Media")
        with tags.tbody():
            for wordle in wordles:
                with tags.tr():
                    tab_wordle(wordle)
                    tab_code(wordle)
                    tags.td(wordle['emoji'])
                    tags.td(raw(wordle['domain']))
                    tab_media(wordle)

def eng_twist(wordles):
    with tags.table(cls='pure-table twisttable'):
        tags.caption("🃏 English wordles with a gameplay twist or variation")
        with tags.thead():
            with tags.tr():
                tags.th("Name")
                tags.th("Code")
                tags.th("Emoji")
                tags.th("Variation")
        with tags.tbody():
            for wordle in wordles:
                with tags.tr():
                    tab_wordle(wordle)
                    tab_code(wordle)
                    tags.td(wordle['emoji'])
                    tags.td(wordle['twist'])

def eng_reimplementations(wordles):
    with tags.table(cls='pure-table reimplementationtable'):
        tags.caption("🧑‍💻 Reimplementations of Wordle (English)")
        with tags.thead():
            with tags.tr():
                tags.th("Name")
                tags.th("Code")
                tags.th("Interface")
                tags.th("Development")
                tags.th("Note")
        with tags.tbody():
            for wordle in wordles:
                with tags.tr():
                    tab_wordle(wordle)
                    tab_code(wordle)
                    tags.td(wordle['interface'])
                    tags.td(wordle['dev'])
                    tab_note(wordle)

def nonling(wordles):
    with tags.table(cls='pure-table nonlinguistictable'):
        tags.caption("😶 Non-linguistic ‘wordles’")
        with tags.thead():
            with tags.tr():
                tags.th("Name")
                tags.th("Code")
                tags.th("Emoji")
                tags.th("Object of guessing")
        with tags.tbody():
            for wordle in wordles:
                with tags.tr():
                    tab_wordle(wordle)
                    tab_code(wordle)
                    tags.td(wordle['emoji'])
                    tags.td(wordle['object'])

def bottomtext():
    with tags.div(style="margin-top: 3em; text-align: center"):
        tags.img(src="wordle.svg", style="width: 80%")
        with tags.p(__pretty=False):
            tags.span("A ")
            tags.a("word cloud", href="https://en.wikipedia.org/wiki/Tag_cloud")
            tags.span(" (a.k.a. ")
            tags.em("wordle")
            tags.span(") made with ")
            tags.a("Jason Davies", href="https://www.jasondavies.com/")
            tags.span("’s open source")
            tags.a(" generator", href="https://www.jasondavies.com/wordcloud/")
            tags.span(".")

def main():
    langs, mlwordles, domwordles, twistwordles, reimplwordles, nonlingwordles = setup()
    doc = dominate.document(title='Wordles of the World')
    header(doc)
    with doc.body:
        with tags.div(cls='content-wrapper'):
            with tags.div(cls='content'):
                toptext(langs, mlwordles, domwordles, twistwordles, reimplwordles, nonlingwordles)
                mltable(langs, mlwordles)
                eng_domain_specific(domwordles)
                eng_twist(twistwordles)
                eng_reimplementations(reimplwordles)
                nonling(nonlingwordles)
                bottomtext()
    with doc:
        tags.script("twemoji.parse(document.body);")
    print(doc)

if __name__=="__main__":
    main()
